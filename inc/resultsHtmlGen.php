<?php

/**
 * resultsHtmlGen generates html tables and or complete result page 
 * from a results object.
 *
 * @author Magnus Söderling
 */
require_once dirname(__FILE__) . '/results.php';

class resultsHtmlGen {

    public $res;

    function __construct() {
        $this->res = new results();
    }

    public function getResByClass($classes) {

        $tmpC = (is_array($classes) ? $classes : Array($classes));

        $posClass = Array();
        foreach ($tmpC as $cl) {
            $clLowCase = strtolower($cl);
            $posClass[] = $clLowCase;
            $posClass[] = $clLowCase . 'j';
            $posClass[] = $clLowCase . ' j';
        }

        $tmpRes = Array();
        $classCol = $this->res->classCol;
        foreach ($this->res->results as $line) {
            if (in_array(strtolower($line[$classCol]), $posClass, TRUE)) {
                $tmpRes[] = $line;
            }
        }
        $totCol = $this->res->totCol;
        $foRange = range($totCol + 1, $totCol + 5);

        $foRTrue = array_map(function($a) {
            return [$a, TRUE];
        }, $foRange);

        $sortArr = array_merge([[$totCol, TRUE]], $foRTrue, [4, 5]);

        return $this->sortResult($tmpRes, $sortArr);
    }

    function getResPlaces($classes) {
        $res = $this->addPlaces($classes);

        $prevLine = NULL;
        foreach ($res as & $curr) {
            if ($prevLine) {
                $eq = $this->resLineIsEqual($curr, $prevLine, $this->res->totCol);
                if ($eq) {
                    $curr[9999] = $prevLine[9999];
                }
            }
            $prevLine = $curr;
        }
        return $res;
    }

    private function addPlaces($classes) {
        $res = $this->getResByClass($classes);
        $pl = 1;
        foreach ($res as & $line) {
            $line[9999] = $pl++; //should be a constant
        }
        return $res;
    }

    public function htmlTable($hdr, $res, $class = 'result') {
        $keys = $this->usedKeys($res);

        echo '<table class="' . $class . '">' . "\n";
        $this->printTrow($hdr, $keys, $hdr);
        foreach ($res as $line) {
            $this->printTrow($line, $keys, $hdr);
        }
        echo '</table>' . "\n";
    }

    private function printTrow($row, $keys, $hdr) {
        $tmpStr = '';
        foreach ($keys as $key) {
            if (isset($row[$key])) {
                $tmpStr = $tmpStr . '<td class="' . $hdr[$key];
                $tmpStr = $tmpStr . '">' . $row[$key] . '</td>';
            }
        }
        print("<tr>$tmpStr</tr>\n");
    }

    private function resLineIsEqual($a, $b, $totCol) {
        if ($a[$totCol] === $b[$totCol]) {
            //print("$a[$totCol]   $b[$totCol]");
            $foCol = $totCol + 1;
            $isEq = TRUE;
            while ($isEq) {

                $aIsSet = isset($a[$foCol]);
                $bIsSet = isset($b[$foCol]);

                if ($aIsSet && $bIsSet) {
                    $isEq = ($a[$foCol] === $b[$foCol]);
                    $foCol++;
                } else {
                    if ($aIsSet || $bIsSet) {//exactly one is set!
                        return FALSE;
                    } else {
                        return TRUE;
                    }
                }
            }
            return $isEq;
        } else {
            return FALSE;
        }
    }

    private function sortResult(array $data, array $fields) {
        usort($data, function($a, $b) use($fields) {
            $retval = 0;
            foreach ($fields as $fieldOpt) {
                if ($retval == 0) {
                    if (isset($fieldOpt[1]) && $fieldOpt[1]) {//Descending
                        $retval = strnatcmp($b[$fieldOpt[0]], $a[$fieldOpt[0]]);
                    } else {
                        $retval = strnatcmp($a[$fieldOpt], $b[$fieldOpt]);
                    }
                }
            } return $retval;
        });
        return $data;
    }

    private function usedKeys($data) {

        $tmpKeys = array();
        foreach ($data as $line) {
            $tmpKeys = array_merge($tmpKeys, array_keys(array_filter($line, 'strlen')));
            $tmpKeysUniq = array_unique($tmpKeys);
        }
        asort($tmpKeysUniq);
        return $tmpKeysUniq;
    }

}
