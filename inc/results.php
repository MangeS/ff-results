<?php

/**
 * results store and read results
 *
 * @author Magnus Söderling
 */

require_once dirname(__FILE__) . '/../lib/spreadsheet-reader/SpreadsheetReader.php';

class results {

    public $classes = array();
    public $res_header = NULL;
    public $results = NULL;
    public $classCol = NULL;
    public $totCol = NULL;

    public function readResults($fileName) {
        $reader = new SpreadsheetReader($fileName);
        $this->results = array();
        $header = $reader->current();

        foreach ($header as $key => $value) {
            if (is_null($value) || $value == '') {
                unset($header[$key]);
            } else {
                if (strncasecmp($value, 'klass', 5) === 0 ||
                        strncasecmp($value, 'class', 5) === 0) {
                    $this->classCol = $key;
                } elseif (strncasecmp($value, 'tot', 3) === 0) {
                    $this->totCol = $key;
                }
            }
        }

        $this->res_header = $header;
//        $hdr_keys = array_keys($header);

        foreach ($reader as $row) {
            $tmp_row = Array();
            foreach (array_keys($header) as $key) {
                $tmp_row[$key] = $row[$key];
            }
            if (array_filter($tmp_row)) {//Remove empty arrays
                $this->results[] = $tmp_row;
                $this->classes[] = $tmp_row[$this->classCol];
            }
        }
        $this->classes = array_unique($this->classes);
        sort($this->classes);
    }

}
