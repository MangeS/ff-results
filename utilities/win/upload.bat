@echo off
set SAVENAME=Majt2014_%DATE:/=-%_%TIME::=-%
set SAVENAME=%SAVENAME: =%.ods

echo Uploading with name:"%SAVENAME%"

pscp.exe -P 33989 -i MajtKey.ppk majt2014.ods mange@majt.noip.me:webdev/majt.noip.me/public/%SAVENAME%
ping 1.1.1.1 -n 1 -w 5000 > nul