<!DOCTYPE html>
<?php
require_once '../inc/results.php';
require_once '../inc/resultsHtmlGen.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Resultat Majtävlingen 2014</title>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <h1>Majtävlingen 2014</h1>
        <?php
        $files = glob("*.ods");
        $filesMod = array_combine($files, array_map("filemtime", $files));
        arsort($filesMod);
        $latest_file = key($filesMod);

        if (is_null($latest_file)) {
            echo '<h1>could not find any *.ods file terminating script</h1>';
        } else {


            $res = new results();
            $res->readResults($latest_file);
            
//            foreach ($res->results as $value) {
//            var_dump($value);    
//            }
            

            $resHTML = new resultsHtmlGen();
            $resHTML->res = $res;
            $usedClasses = $res->classes;

            foreach ($usedClasses as $cl) {
                $kl = ($cl === '') ? 'Klass saknas' : $cl;
                echo '<h1 class="result">' . $kl . '</h1>' . "\n";
                $rhdr = $resHTML->res->res_header;
                $rhdr[9999] = 'Pl';//9999 should be a constant
                //$ccls = $resHTML->getResByClass($cl);
                $ccls = $resHTML->getResPlaces($cl);
                //var_dump($rhdr);
                $resHTML->htmlTable($rhdr, $ccls);
            }

            $lnkStr = '"><h1>Kalkylbladet som ovanstående baseras på</h1></a>';
            echo '<a href="' . $latest_file . $lnkStr;
        }
        ?>

    </body>
</html>
